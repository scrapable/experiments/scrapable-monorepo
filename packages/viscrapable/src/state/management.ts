import { AppState } from "./types"
import { isElectron } from "../utils"
import createEngine, { CDPCommand } from "@scrpbl/engine"

export type SetStateFn = (newState: AppState) => any

export const init = (state: AppState, setState: SetStateFn) => {
	if(!isElectron) return
	const webview = document.getElementById("webview")
	webview.addEventListener("dom-ready", async () => {
		const dbugger = (webview as any).getWebContents().debugger
		dbugger.attach()
		const transportToDebugger = (command: CDPCommand) =>
			dbugger.sendCommand(command.method, command.args)
		const engine = createEngine(transportToDebugger)
		await engine.execute({
			op: "goto",
			args: {
				url: "https://t0ast.cc/about"
			},
			delay: 500
		})
		await engine.execute({
			op: "goto",
			args: {
				url: "https://t0ast.cc/blog"
			},
			delay: 2000
		})
	}, { once: true })
}

export const incCounter = (state: AppState, setState: SetStateFn) => {
    setState({
        ...state,
        counter: state.counter + 1
    })
}
