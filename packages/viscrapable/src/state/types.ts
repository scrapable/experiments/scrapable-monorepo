import { ScrapableEvent } from "@scrpbl/engine"

export class AppState {
    constructor(
        public readonly counter: number = 0,
        public readonly eventQueue: Array<ScrapableEvent> = []
    ) { }
}
