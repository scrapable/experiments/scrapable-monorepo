import * as React from "react"
import { Component } from "react"
import { hot } from "react-hot-loader"
import Thing from "./Thing"
import { isElectron } from "../utils"
import { AppState } from "../state/types"
import { init, incCounter } from "../state/management"

class App extends Component<{}, AppState> {
	state = new AppState()

	render() {
		const state = this.state
		const setState = this.setState.bind(this)

		return (
			<div>
				<h1>Hello world from {isElectron ? "Electron" : "browser"}</h1>
				<p>Counter is {this.state.counter}</p>
				<button onClick={() => incCounter(state, setState)}>Inc</button>
				<Thing />
			</div>
		)
	}

	async componentDidMount() {
		await init(this.state, this.setState.bind(this))
	}
}

export default hot(module)(App)
