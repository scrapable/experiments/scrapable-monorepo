// The next two lines are necessary because TypeScript sucks
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../node_modules/@types/jest/index.d.ts" />


const noopTransport = jest.fn()

test("simple goto", async () => {
	const engine = createEngine(noopTransport)
	await engine.execute({
		op: "goto",
		args: {
			url: "https://duck.com"
		},
		delay: 100
	})
	expect(noopTransport).toBeCalledWith(new CDPCommand(
		"Page.navigate",
		{ url: "https://duck.com" }))
})
